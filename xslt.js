const xsltproc = require('xsltproc');

const xsl  = process.argv[2];
const src  = process.argv[3];
const dest = process.argv[4];

const xslt = xsltproc.transform(xsl, src, {
    profile: true,
    output: dest
});

xslt.stderr.on('data', function (data) {
    console.log('xsltproc stderr: ' + data);
});

xslt.on('exit', function (code) {
    console.log('xsltproc process exited with code ' + code);
});
