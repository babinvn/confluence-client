const https = require('https');
const axios = require('axios');
const qs = require('qs');
const agent = new https.Agent({ rejectUnauthorized: false });

async function get(options, url, params) {
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await axios.get(`${options.baseaddr}${url}`,
      {
        params,
        auth: {
          username: options.username,
          password: options.password
        },
        httpsAgent: agent,
        headers: { 'x-atlassian-token': 'no-check' },
        paramsSerializer: params => qs.stringify(params, { arrayFormat: 'repeat' })
      });
      resolve(resp.data);
    } catch(err) {
      reject(err);
    }
  })
}
async function put(options, url, data){
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await axios.put(`${options.baseaddr}${url}`,
        data,
        {
          auth: {
            username: options.username,
            password: options.password
          },
          httpsAgent: agent,
          headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'x-atlassian-token': 'no-check'
          }
        }
      );
      resolve(resp.data);
    } catch(err) {
      reject(err);
    }
  })
}
async function putXml(options, url, data){
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await axios.put(`${options.baseaddr}${url}`,
        data,
        {
          auth: {
            username: options.username,
            password: options.password
          },
          httpsAgent: agent,
          headers: {
            'Content-Type': 'text/xml;charset=utf-8',
            'x-atlassian-token': 'no-check'
          }
        }
      );
      resolve(resp.data);
    } catch(err) {
      reject(err);
    }
  })
}
async function post(options, url, data){
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await axios.post(`${options.baseaddr}${url}`,
        data,
        {
          auth: {
            username: options.username,
            password: options.password
          },
          httpsAgent: agent,
          headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'x-atlassian-token': 'no-check'
          }
        }
      );
      resolve(resp.data);
    } catch(err) {
      reject(err);
    }
  })
}
async function postForm(options, url, data){
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await axios.post(`${options.baseaddr}${url}`,
        qs.stringify(data),
        {
          auth: {
            username: options.username,
            password: options.password
          },
          httpsAgent: agent,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-atlassian-token': 'no-check'
          }
        }
      );
      resolve(resp.data);
    } catch(err) {
      reject(err);
    }
  })
}

async function del(options, url, params) {
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await axios.delete(`${options.baseaddr}${url}`,
      {
        params,
        auth: {
          username: options.username,
          password: options.password
        },
        httpsAgent: agent,
        headers: { 'x-atlassian-token': 'no-check' }
      });
      resolve(resp.data);
    } catch(err) {
      reject(err);
    }
  })
}

function Client(options) {
  this.options = options;
}

Client.prototype.get = async function (url, params) {
  return get(this.options, url, params);
}
Client.prototype.post = async function (url, data) {
  return post(this.options, url, data);
}
Client.prototype.postForm = async function (url, data) {
  return postForm(this.options, url, data);
}
Client.prototype.put = async function (url, data) {
  return put(this.options, url, data);
}
Client.prototype.putXml = async function (url, data) {
  return putXml(this.options, url, data);
}
Client.prototype.del = async function (url, params) {
  return del(this.options, url, params);
}

Client.prototype.queryPages = async function (cql, start) {
  const params = { cql, limit:500 };
  if (start)
    params.start = start;
  return get(this.options, '/rest/api/content/search', params);
}
Client.prototype.queryAllPages = async function (cql) {
  const data = await this.queryPages(cql);
  if (data.totalSize > 500) {
    const results = [];
    Array.prototype.push.apply(results, data.results);
    const requests = [];
    for (let i = 500; i < data.totalSize; i+=500) {
      requests.push(this.queryPages(cql, i));
    }
    const responses = await Promise.all(requests);
    responses.forEach(data => Array.prototype.push.apply(results, data.results));
    return results;
  } else {
    return data.results;
  }
}
Client.prototype.listPagesByLabel = async function(label) {
  return this.queryAllPages(`label="${label}"`);
}
Client.prototype.getPageVersion = async function (pageId) {
  return get(this.options, `/rest/api/content/${pageId}`);
}
Client.prototype.getPageBody = async function (pageId) {
  return get(this.options, `/rest/prototype/1/content/${pageId}`);
}
Client.prototype.putPageBody = async function (pageId, version, body) {
  return put(this.options, `/rest/api/content/${pageId}`,
  {
    type: 'page',
    version: {
      number: version
    },
    body: {
      storage: {
        value: body,
        representation: 'storage'
      }
    }
  });
}
Client.prototype.createPage = async function (spaceKey, parentId, title, body) {
  return post(this.options, `/rest/api/content/`, {
    'type':'page',
    'title':title,
    'space':{'key':spaceKey},
    'ancestors':[{'type':'page','id':parentId}],
    'body':{
      'storage':{
        'value':body,
        'representation':'storage'
      }
    }
  });
}
Client.prototype.delPage = async function (pageId) {
  return this.del(`/rest/api/content/${pageId}`);
}
Client.prototype.getLabels = async function (pageId) {
  return get(this.options, `/rest/api/content/${pageId}/label/`);
}
Client.prototype.postLabel = async function (pageId, label) {
  return post(this.options, `/rest/api/content/${pageId}/label`, label);
}
Client.prototype.listComments = async function (pageId) {
  return get(this.options, `/rest/api/content/${pageId}/child/comment?limit=999`);
}
Client.prototype.deleteComment = async function (commentId) {
  return del(this.options, `/rest/api/content/${commentId}`);
}
Client.prototype.listUsers = async function () {
  return get(this.options, `/rest/user-management/1.0/users?startIndex=0&maxResults=500`);
}
Client.prototype.getUserDetail = async function (login) {
  return get(this.options, `/rest/api/user?username=${login}`);
}
Client.prototype.postContentProperty = async function(contentId, key, value) {
  return post(this.options, `/rest/api/content/${contentId}/property`, {
    key,
    value: {
      anything: value
    }
  });
}
Client.prototype.createSpace = async function (key, name, desc) {
  return post(this.options, `/rest/api/space`, {
    key, name, description: {
      plain: {
        value: desc, representation: 'plain'
      }
    }
  });
}
Client.prototype.deleteSpace = async function (key) {
  return del(this.options, `/rest/api/space/${key}`);
}

// REST API Extensions
Client.prototype.createUser = async function (data) {
  return post(this.options, `/rest/bvnconf/1.0/create-user`, data);
}
// data = {title, description, content, spaceKey, labels}
Client.prototype.createTemplate = async function (data) {
  return post(this.options, `/rest/bvnconf/1.0/template`, data);
}
Client.prototype.putPageBodyAlt = async function (pageId, body, suppressEvents) {
  return putXml(this.options, `/rest/bvnconf/1.0/page/${pageId}?suppress-events=${suppressEvents}`, body);
}
Client.prototype.restorePage = async function(pageId) {
  return this.post(`/rest/bvnconf/1.0/page/restore/${pageId}`);
}
Client.prototype.restorePageFromTrash = Client.prototype.restorePage;
Client.prototype.listUserMacros = async function () {
  return this.get(`/rest/bvnconf/1.0/user-macro`);
}
Client.prototype.getUserMacro = async function (name) {
  return this.get(`/rest/bvnconf/1.0/user-macro/${name}`);
}
Client.prototype.putUserMacro = async function (name, macro) {
  return this.put(`/rest/bvnconf/1.0/user-macro/${name}`, macro);
}
Client.prototype.listBeans = async function () {
  return this.put(`/rest/bvnconf/1.0/singletons`);
}
Client.prototype.getBean = async function (name) {
  return this.put(`/rest/bvnconf/1.0/singletons/${name}`);
}

module.exports = Client;
