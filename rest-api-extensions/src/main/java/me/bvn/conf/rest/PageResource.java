package me.bvn.conf.rest;


import com.atlassian.confluence.core.DefaultSaveContext;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Scanned
@Path("/page")
public class PageResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(PageResource.class);

    @ComponentImport
    private final PageManager pageManager;
    @ComponentImport
    private final PermissionManager permissionManager;

    @Path("/restore/{id}")
    @POST
    public Response restore(@PathParam("id") Long pageId) {
        LOGGER.trace(String.format("Restore page %d", pageId));
        Page page = pageManager.getPage(pageId);
        if (page != null && page.isDeleted()) {
            if (!permissionManager.hasPermission(AuthenticatedUserThreadLocal.get(), Permission.ADMINISTER, page.getSpace())) {
                return Response.status(Response.Status.FORBIDDEN).build();
            }
            pageManager.restorePage(page);
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity("Page not found in trash").build();
        }
    }

    @Path("/{id}")
    @PUT
    public Response put(@PathParam("id") Long pageId, @QueryParam("suppress-events") boolean suppressEvents, String body){
        Page page = pageManager.getPage(pageId);
        if (page == null)
            return Response.status(Response.Status.NOT_FOUND).entity(String.format("Page could not be found: %d", pageId)).build();

        if (body == null || body.isEmpty())
            return Response.status(Response.Status.NO_CONTENT).entity("Page content cannot be empty").build();

        if (body.equals(page.getBodyAsString()))
            return Response.status(Response.Status.CONFLICT).entity("Page content has not changed").build();

        DefaultSaveContext sc = new DefaultSaveContext(true/*suppressNotifications*/, true/*updateLastModifier*/, suppressEvents);
        pageManager.saveNewVersion(page, (Page p) -> {
            p.setBodyAsString(body);
            LOGGER.warn(String.format("Created new version of page %d", pageId));
        }, sc);

        return Response.status(Response.Status.ACCEPTED).build();
    }
    
    @Inject
    public PageResource(PageManager pageManager, PermissionManager permissionManager) {
        this.pageManager = pageManager;
        this.permissionManager = permissionManager;
    }
}