package me.bvn.conf.rest;

import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.user.security.password.Credential;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.node.ObjectNode;

@Path("/create-user")
@Scanned
public class CreateUserResource {
    @ComponentImport
    private final UserAccessor userAccessor;

    @POST
    public Response getMessage(ObjectNode user) {
        if (userAccessor.exists(user.get("username").asText())){
            return Response.status(Response.Status.OK).build();
        }
        ConfluenceUserImpl u = new ConfluenceUserImpl(
            user.get("username").asText(),
            user.get("displayName").asText(),
            user.get("email").asText()
        );
        try {
            userAccessor.createUser(u, Credential.unencrypted("pifagor"));
            return Response.status(Response.Status.CREATED).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }
    
    @Inject
    public CreateUserResource(UserAccessor userAccessor){
        this.userAccessor = userAccessor;
    }
}