package me.bvn.conf.rest;

import com.atlassian.confluence.setup.ConfluenceListableBeanFactory;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.spring.container.SpringContainerContext;
import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Arrays;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

@Path("/singletons")
public class BeansResource {
    private final ObjectMapper mapper = new ObjectMapper();

    @GET
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSingletons() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        ConfluenceListableBeanFactory beanFactory = getGlobalBeanFactoryUsingHacks();
        ArrayNode arr = mapper.createArrayNode();
        Arrays.asList(beanFactory.getSingletonNames()).forEach(name -> arr.add(name));
        return Response.ok(arr).build();
    }

    @GET
    @Path("/{name}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSingleton(@PathParam("name") String name) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Object component = ContainerManager.getComponent(name);
        ObjectNode obj = mapper.createObjectNode();
        obj.put("class", component.getClass().getName());
        ArrayNode arr = mapper.createArrayNode();
        for (Class iface : obj.getClass().getInterfaces()){
            arr.add(iface.getName());
        }
        obj.put("interfaces", arr);
        return Response.ok(obj).build();
    }

    private ConfluenceListableBeanFactory getGlobalBeanFactoryUsingHacks() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Object applicationContext = getApplicationContextWithHack();
        Field field = applicationContext.getClass() // Only reflection
                .getSuperclass()
                .getSuperclass()
                .getSuperclass()
                .getSuperclass()
                .getDeclaredField("beanFactory");
        field.setAccessible(true);
        Object obj = AccessController.doPrivileged((PrivilegedAction) () -> {
            try {
                return field.get(applicationContext);
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        });
        return (ConfluenceListableBeanFactory)obj;
    }

    private Object getApplicationContextWithHack() {
        ContainerContext containerContext = ContainerManager.getInstance().getContainerContext();
        if (containerContext instanceof SpringContainerContext) {
            SpringContainerContext springContainerContext = (SpringContainerContext) containerContext;
            ServletContext servletContext = springContainerContext.getServletContext();
            return servletContext.getAttribute("org.springframework.web.context.WebApplicationContext.ROOT");
        } else {
            return null;
        }
    }
}