package me.bvn.conf.rest;


import com.atlassian.confluence.api.model.content.Label;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.BodyType;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.templates.PageTemplate;
import com.atlassian.confluence.pages.templates.PageTemplateManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.LabelUtil;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Scanned
@Path("/template")
public class TemplateResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(TemplateResource.class);

    @ComponentImport
    private final SpaceManager spaceManager;
    @ComponentImport
    private final PageTemplateManager pageTemplateManager;
    @ComponentImport
    private final LabelManager labelManager;

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response post(TemplateResourceModel model) {
        try {
            PageTemplate pageTemplate = createPageTemplate(model);
            if (model.getSpaceKey() != null) {
                Space space = spaceManager.getSpace(model.getSpaceKey());
                space.addPageTemplate(pageTemplate);
            }
            pageTemplateManager.savePageTemplate(pageTemplate, null);
            if (model.getLabels() != null) {
                List<Label> labels = new ArrayList<>();
                Arrays.asList(model.getLabels()).forEach(label -> {
                    labels.add(Label.builder(label).build());
                });
                setLabelsOnTemplate(pageTemplate, labels);
            }
            TemplateResourceResponse response = new TemplateResourceResponse(pageTemplate.getId());            
            return Response.status(Response.Status.CREATED).entity(response).build();
        } catch (Throwable ex) {
            LOGGER.error("Failed to create template", ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

    private PageTemplate createPageTemplate(TemplateResourceModel model) throws XhtmlException {
        PageTemplate template = new PageTemplate();

        template.setName(model.getTitle());
        template.setDescription(model.getDescription());
        template.setContent(model.getContent());
        template.setBodyType(BodyType.XHTML);

        return template;
    }
    protected void setLabelsOnTemplate(PageTemplate template, List<Label> labels) {
        LabelUtil.syncState(labels, labelManager, AuthenticatedUserThreadLocal.get(), template);
    }

    @Inject
    public TemplateResource(
        SpaceManager spaceManager, PageTemplateManager pageTemplateManager,
        LabelManager labelManager
    ) {
        this.spaceManager = spaceManager;
        this.pageTemplateManager = pageTemplateManager;
        this.labelManager = labelManager;
    }
}