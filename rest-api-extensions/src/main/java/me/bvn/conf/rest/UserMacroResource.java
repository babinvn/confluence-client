package me.bvn.conf.rest;

import com.atlassian.confluence.renderer.UserMacroConfig;
import com.atlassian.confluence.renderer.UserMacroLibrary;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.spring.container.ContainerManager;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

@Path("/user-macro")
@Scanned
public class UserMacroResource {
    private final ObjectMapper mapper = new ObjectMapper();

    @GET
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON})
    public Response get() {
        UserMacroLibrary userMacroLibrary = (UserMacroLibrary)ContainerManager.getComponent("userMacroLibrary");
        return Response.ok(userMacroLibrary.getMacroNames()).build();
    }
    
    @GET
    @Path("/{name}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response get(@PathParam("name") String name) {
        UserMacroLibrary userMacroLibrary = (UserMacroLibrary)ContainerManager.getComponent("userMacroLibrary");
        UserMacroConfig config = userMacroLibrary.getMacro(name);
        ObjectNode obj = mapper.createObjectNode();
        obj.put("bodyType", config.getBodyType());
        obj.put("description", config.getDescription());
        obj.put("documentationUrl", config.getDocumentationUrl());
        obj.put("iconLocation", config.getIconLocation());
        obj.put("name", config.getName());
        obj.put("template", config.getTemplate());
        obj.put("title", config.getTitle());
        obj.put("hasBody", config.isHasBody());
        obj.put("hidden", config.isHidden());
        obj.putPOJO("categories", config.getCategories());
/*
        ArrayNode params = mapper.createArrayNode();
        config.getParameters().forEach(parameter -> {
            ObjectNode param = mapper.createObjectNode();
            param.put("defaultValue", parameter.getDefaultValue());
            param.put("name", parameter.getName());
            param.putPOJO("aliases", parameter.getAliases());
            param.put("description", parameter.getDescription().toString());
            param.put("displayName", parameter.getDisplayName().toString());
            params.add(param);
        });
        obj.put("parameters", params);
*/
        return Response.ok(obj).build();
    }
    
    @PUT
    @Path("/{name}")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response get(@PathParam("name") String name, ObjectNode obj) {
        UserMacroLibrary userMacroLibrary = (UserMacroLibrary)ContainerManager.getComponent("userMacroLibrary");
        UserMacroConfig config = new UserMacroConfig();
        if (obj.has("bodyType") && !obj.get("bodyType").isNull())
            config.setBodyType(obj.get("bodyType").asText());
        if (obj.has("description") && !obj.get("description").isNull())
            config.setDescription(obj.get("description").asText());
        if (obj.has("documentationUrl") && !obj.get("documentationUrl").isNull())
            config.setDocumentationUrl(obj.get("documentationUrl").asText());
        if (obj.has("iconLocation") && !obj.get("iconLocation").isNull())
            config.setIconLocation(obj.get("iconLocation").asText());
        if (obj.has("name") && !obj.get("name").isNull())
            config.setName(obj.get("name").asText());
        if (obj.has("template") && !obj.get("template").isNull())
            config.setTemplate(obj.get("template").asText());
        if (obj.has("title") && !obj.get("title").isNull())
            config.setTitle(obj.get("title").asText());
        if (obj.has("hasBody") && !obj.get("hasBody").isNull())
            config.setHasBody(obj.get("hasBody").asBoolean());
        if (obj.has("hidden") && !obj.get("hidden").isNull())
            config.setHidden(obj.get("hidden").asBoolean());
        if (obj.has("categories") && obj.get("categories").isArray()) {
            ArrayNode categories = (ArrayNode)obj.get("categories");
            Set<String> cats = new HashSet<>();
            categories.forEach(a -> {
                if (a.isTextual())
                    cats.add(a.asText());
            });
            config.setCategories(cats);
        }        
        userMacroLibrary.addUpdateMacro(config);
        return Response.status(Response.Status.ACCEPTED).build();
    }
}