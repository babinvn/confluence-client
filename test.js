/*
 * To run this test please create file .test.settings.js
 * that may look like follows:
 *
module.exports = {
  baseaddr: process.env.TEST_WIKI_HOME,
  username: process.env.TEST_WIKI_USER,
  password: process.env.TEST_WIKI_PASSWORD,
  sampleSpace: 'TEST123'
}
 */

const fs = require('fs');
const { join } = require('path');
const soynode = require('soynode');
const xsltproc = require('xsltproc');
const Client = require('./index');
const options = require('./.test.settings.js');
const { delay } = require('./util');

/*
soynode.setOptions({
  outputDir: './temp', allowDynamicRecompile: true
});
*/
async function compileTemplates() {
  return new Promise((resolve, reject) => {
    soynode.compileTemplates(join(__dirname, 'test', 'templates'), async function (err){
      if (err) {
        reject(err);
      } else {
        resolve(soynode);
      }
    });
  });
}
function expandBodyXML(body){
  return body.replace(/<table/, [
    '<?xml version="1.0" encoding="UTF-8"?>',
    '<!DOCTYPE page [',
    '<!ENTITY nbsp \"&#160;\">',
    '<!ENTITY laquo \"&#171;\">',
    '<!ENTITY raquo \"&#187;\">',
    '<!ENTITY ndash \"&#8211;\">',
    '<!ENTITY mdash \"&#8212;\">',
    '<!ENTITY ldquo \"&#8220;\">',
    '<!ENTITY rdquo \"&#8221;\">',
    '<!ENTITY sbquo \"&#8218;\">',
    '<!ENTITY eacute \"&#233;\">',
    ']>',
    '<table',
    'xmlns="http://www.w3.org/1999/xhtml"',
    'xmlns:xsl="http://www.w3.org/1999/XSL/Transform"',
    'xmlns:xhtml="http://www.w3.org/1999/xhtml"',
    'xmlns:ac="http://www.atlassian.com/schema/confluence/4/ac/"',
    'xmlns:at="http://www.atlassian.com/schema/confluence/4/at/"',
    'xmlns:ri="http://www.atlassian.com/schema/confluence/4/ri/"',
    'xmlns:acxhtml="http://www.atlassian.com/schema/confluence/4/"',
    ''
  ].join(' '));
}
function collapseBodyXML(body){
  return body.replace(/<ac:layout (.+?)>/g, '<ac:layout>');
}
function transform(src, dst) {
  var xslt = xsltproc.transform('test/company.xslt', src, {
    output: dst
  });
  return new Promise(resolve => {
    xslt.on('exit', function(code) {
      if (code === 0){
        resolve();
      } else {
        throw new Error(`XSL transformer exited with code: ${code}`);
      }
    });
  });
}

describe('Confluence Client Test', () => {
/*
  beforeAll(async () => {});
*/
  beforeEach(async () => {
    jest.setTimeout(100000);
  });

  it('Batch create pages from JSON data using soy tempate then update using xslt template',
    async () => new Promise(async (resolve, reject) => {
      const client = new Client(options);
      try {
        await client.deleteSpace(options.sampleSpace);
        console.debug(`Space ${options.sampleSpace} successfully deleted. This task may take some time to complete...`);
        await delay(5000);
      } catch(ignore) {
      }

      try {
        const space = await client.createSpace(options.sampleSpace, 'Sample Data', 'This space contains sample data and can be freely deleted');
        console.debug(`Space ${space.name} (re)created successfully`)

        // Create template
        const content = fs.readFileSync(join('test', 'templates', 'company.xml'), 'utf8');
        const template = await client.createTemplate({
          title: 'Sample Data Template',
          description: 'Company financial information demo template',
          content,
          spaceKey: space.key,
          labels: [ 'sample-company' ]
        });
        console.debug(`Template ${template.id} (re)created successfully`)

        // Create pages
        const data = JSON.parse(fs.readFileSync('./test/data.json', 'utf-8'));
        const soynode = await compileTemplates();

        for (let i = 0; i < data.length; i++) {
          const rec = data[i];
          const title = rec.title;
          const body = soynode.render('Templates.company', { data: rec });
          const resp = await client.createPage(space.key, space.homepage.id, title, body);
          const pageId = resp.id;
          await client.postLabel(pageId, { prefix: 'global', name: 'sample-company' });
          console.debug(`Created page ${title}`);
        }

        // Update pages
        /*
        const pages = await client.listPagesByLabel('sample-company');
        for (let i = 0; i < pages.length; i++) {
          const page = pages[i];
          const resp = await client.getPageBody(page.id);
          const expandedBody = expandBodyXML(resp.body.value);
          fs.writeFileSync('.old.xml', expandedBody, 'utf8');
          await transform('.old.xml', '.new.xml');
          const newBody = collapseBodyXML(fs.readFileSync('.new.xml', 'utf8'));
          console.debug(`Updating page ${page.id}: ${page.title}`);
          await client.putPageBodyAlt(page.id, '2', newBody);
        }
        */


        const pageTitle = 'Sinopec';
        const pages = await client.queryAllPages(`space="${space.key}" and title="${pageTitle}"`);
        expect(pages.length).toBe(1);

        await client.delPage(pages[0].id);
        console.debug(`Trashed page ${pages[0].title}`);

        await client.restorePage(pages[0].id);
        console.debug(`Restored page ${pages[0].title}`);

        resolve();
      } catch(err) {
        reject(err);
      }
    })
  );
  it('Delete and restore a page',
    async () => new Promise(async (resolve, reject) => {
      const client = new Client(options);

      await client.delPage(4522408);
      await client.restorePage(4522408);

      resolve();
    })
  );

});
